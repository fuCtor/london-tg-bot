enablePlugins(DockerPlugin, AshScriptPlugin)

name := "london_bot"

version := "0.1"

scalaVersion := "2.12.10"

resolvers += "JCenter" at "https://jcenter.bintray.com"

dockerBaseImage := "openjdk:8-jre-alpine"
scriptClasspath := "/${app_home}/../resources" +: scriptClasspath.value

val finagleVersion = "19.9.0"
val catsVersion = "2.0.0"
val monixVersion = "3.0.0"
val circeVersion = "0.11.1"

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-language:experimental.macros",
  "-feature",
  "-unchecked",
  "-Xfatal-warnings",
  "-Xlint",
  "-Ywarn-numeric-widen",
  "-Ywarn-value-discard",
  "-Ywarn-dead-code", // Warn when dead code is identified.
  "-Ywarn-extra-implicit", // Warn when more than one implicit parameter section is defined.
  "-Ywarn-inaccessible", // Warn about inaccessible types in method signatures.
  "-Ywarn-infer-any", // Warn when a type argument is inferred to be `Any`.
  "-Ywarn-nullary-override", // Warn when non-nullary `def f()' overrides nullary `def f'.
  "-Ywarn-nullary-unit", // Warn when nullary methods return Unit.
  "-Ywarn-unused:implicits", // Warn if an implicit parameter is unused.
  "-Ywarn-unused:imports", // Warn if an import selector is not referenced.
  "-Ywarn-unused:locals", // Warn if a local definition is unused.
  "-Ywarn-unused:privates", // Warn if a private member is unused.
)

libraryDependencies ++= Seq(
  "com.bot4s" %% "telegram-core" % "4.4.0-RC1",
  "com.softwaremill.sttp" %% "async-http-client-backend-monix" % "1.6.7",
  "com.twitter" %% "finagle-core" % finagleVersion,
  "com.twitter" %% "finagle-redis" % finagleVersion,

  "io.monix" %% "monix" % monixVersion,
  "org.typelevel" %% "cats-core" % catsVersion,
  "org.typelevel" %% "cats-effect" % catsVersion,

  "org.postgresql" % "postgresql" % "42.2.8",
  "io.getquill" %% "quill-jdbc-monix" % "3.4.8",
  "org.liquibase" % "liquibase-core" % "3.6.1",

  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "com.github.pureconfig" %% "pureconfig" % "0.12.0",
  "org.rudogma" %% "supertagged" % "1.4"
)

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-jackson29"
).map(_ % circeVersion)
