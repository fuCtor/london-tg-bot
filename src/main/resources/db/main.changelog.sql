--liquibase formatted sql

--changeset bot:1

CREATE TABLE tags (
    id SERIAL PRIMARY KEY NOT NULL,
    chat_id int NOT NULL,
    message_id int NOT NULL,
    user_id int NOT NULL,
    tag varchar NOT NULL,
    created_at TIMESTAMP DEFAULT now()
);

CREATE INDEX bot_chat_id_idx ON tags(chat_id);
CREATE INDEX bot_tag_idx ON tags(tag);

--changeset bot:2
ALTER TABLE tags ALTER COLUMN chat_id TYPE bigint;

--changeset bot:3

CREATE TABLE faqs (
    id SERIAL PRIMARY KEY NOT NULL,
    chat_id int NOT NULL,
    caption varchar NOT NULL,
    "text" varchar NOT NULL,
    created_at TIMESTAMP DEFAULT now()
);

CREATE INDEX faq_chat_id_idx ON faqs(chat_id);

--changeset bot:4
ALTER TABLE faqs ALTER COLUMN chat_id TYPE bigint;

-- changelog bot:5

CREATE TABLE yandex_locations (
    id SERIAL PRIMARY KEY NOT NULL,
    chat_id bigint NOT NULL,
    caption varchar NOT NULL,
    latitude real NOT NULL,
    longitude real NOT NULL,
    created_at TIMESTAMP DEFAULT now()
);

CREATE INDEX yal_chat_id_idx ON yandex_locations(chat_id);