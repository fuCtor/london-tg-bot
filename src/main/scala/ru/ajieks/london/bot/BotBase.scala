package ru.ajieks.london.bot

import cats.data.OptionT
import com.bot4s.telegram.api.{BotBase => TelegramBotBase}
import com.bot4s.telegram.cats.Polling
import com.bot4s.telegram.models.User
import ru.ajieks.london.bot.model.{Context, UserState}
import ru.ajieks.london.bot.storage.StateStorage

import scala.language.higherKinds

trait BotBase[F[_]] extends Polling[F] {
  bot : TelegramBotBase[F] =>

  type BotStateF[A] = BotState[F, A]

  final def execute[M, A, R](user: Option[User], state: OptionT[BotStateF, R]): F[Option[R]] = user match {
    case Some(u) =>
      import cats.syntax.applicative._
      import cats.syntax.flatMap._
      stateStorage.get(UserId(u.id))
        .flatMap(ctxOpt => state.value.run(Context[F](bot, ctxOpt.getOrElse(UserState.Empty()))))
        .flatMap({
          case (ctx, r) => stateStorage.put(UserId(u.id), ctx.userState).flatMap(_ => r.pure[F])
        })
    case None => state.value.runA(Context[F](bot, UserState.empty))
  }

  def stateStorage: StateStorage[F]
}
