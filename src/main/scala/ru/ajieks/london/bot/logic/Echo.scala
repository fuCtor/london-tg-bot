package ru.ajieks.london.bot.logic

import cats.Monad
import cats.data.OptionT
import com.bot4s.telegram.methods.SendMessage
import com.bot4s.telegram.models.Message
import ru.ajieks.london.bot.BotState

import scala.language.higherKinds

class Echo[F[_]: Monad] extends Logic[F] {
  override def receiveMessage(message: Message): OptionT[BotStateF, Unit] = OptionT[BotStateF, Unit] {
    BotState.liftA { ctx =>
      message.text match {
        case Some(Echo.rgx(text)) =>
          ctx.bot.request(SendMessage(message.chat.id, text)).accept
        case _ =>
          skip
      }
    }
  }
}

object Echo {
  val rgx = "^\\!echo\\s(.+)$".r

  def apply[F[_]: Monad]: Echo[F] = new Echo
}
