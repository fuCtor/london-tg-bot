package ru.ajieks.london.bot.logic

import cats.Monad
import cats.data.OptionT
import com.bot4s.telegram.methods.{DeleteMessage, SendMessage, SendPhoto}
import com.bot4s.telegram.models.{CallbackQuery, InlineKeyboardButton, InlineKeyboardMarkup, InputFile, Location, Message}
import ru.ajieks.london.bot.{BotState, ChatId}
import ru.ajieks.london.bot.model.YandexLocation
import ru.ajieks.london.bot.storage.YandexLocationStorage

import scala.language.higherKinds

class Yandex[F[_] : Monad](storage: YandexLocationStorage[F]) extends Logic[F] {
  import cats.implicits._

  override def receiveCallbackQuery(callbackQuery: CallbackQuery): OptionT[BotStateF, Unit] = OptionT[BotStateF, Unit] {
    BotState.liftA { ctx =>
      callbackQuery.data.fold(skip)(_.split(':').toList match {
        case "location" :: "show" :: id :: Nil =>
          val source = callbackQuery.message.fold(callbackQuery.from.id.toLong)(_.source)
          callbackQuery.message.fold(true.pure[F])( m =>
            ctx.bot.request(DeleteMessage(source, m.messageId))
          ).flatMap(_ =>
            storage.get(id.toLong).flatMap({
              case Some(loc) =>
                val url = s"https://static-maps.yandex.ru/1.x/?ll=${loc.longitude}%2C${loc.latitude}&spn=0.006,0.006&l=map,trf&randmon=${System.currentTimeMillis()}"

                val msg = SendPhoto(source, InputFile(url), Some(loc.caption))
                ctx.bot.request(msg).accept
              case _ => accept
            })
          )
        case _ => skip
      })
    }
  }

  override def receiveMessage(message: Message): OptionT[BotStateF, Unit] = OptionT[BotStateF, Unit] {
    BotState.liftA { ctx =>
      message.text match {
        case Some("/пробки") =>
          storage.list(ChatId(message.source), 0).flatMap({ locations =>
            println(locations)
            val buttons = locations.map({ loc =>
              InlineKeyboardButton.callbackData(s"🗺 ${loc.caption}", s"location:show:${loc.id.getOrElse(0)}")
            })
            val markup = InlineKeyboardMarkup.singleColumn(buttons)
            ctx.bot.request(SendMessage(message.source, "Что посмотрим❓", replyMarkup = Some(markup))).accept
          })
        case Some(text) if text.startsWith("/пробки") =>
          val caption = text.replace("/пробки", "").trim
          message.replyToMessage.flatMap(_.location).fold(skip)( location =>
            saveLocation(message, caption, location).flatMap( msg =>
              ctx.bot.request(msg)
            ).accept
          )
        case _ =>
          skip
      }
    }
  }

  def saveLocation(message: Message, caption: String, location: Location): F[SendMessage] = {
    val loc = YandexLocation(None, message.source, caption, location.longitude, location.latitude)
    storage.put(loc).flatMap({
      case true => s"Спасибо, запомнил про: ${caption}".pure[F]
      case false => s"Что-то пошло не так 😞, попробуй попозже еще раз".pure[F]
    }).flatMap(msg =>
      SendMessage(message.source, msg, replyToMessageId = Some(message.messageId)).pure[F]
    )
  }
}

object Yandex {
  def apply[F[_] : Monad](storage: YandexLocationStorage[F]): Yandex[F] = new Yandex(storage)
}

