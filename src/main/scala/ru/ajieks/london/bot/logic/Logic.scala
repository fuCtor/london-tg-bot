package ru.ajieks.london.bot.logic

import cats.{Applicative, FlatMap, Id}
import cats.data.{OptionT, ReaderT}
import com.bot4s.telegram.models._
import ru.ajieks.london.bot.BotState
import ru.ajieks.london.bot.model.{Context, UserState}

import scala.language.higherKinds

abstract class Logic[F[_]: Applicative] extends ru.ajieks.london.bot.Logic[F] {
  logic =>
  val unit: OptionT[BotStateF, Unit] = OptionT[BotStateF, Unit](
     BotState.pure[F, Option[Unit]](Option.empty[Unit])
  )

  def skip(ctx: Context[F]): F[(Context[F], Option[Unit])] = Applicative[F].pure(ctx -> Option.empty[Unit])
  val skip: F[Option[Unit]] = Applicative[F].pure(Option.empty[Unit])
  val accept: F[Option[Unit]] = Applicative[F].pure(Option(()))

  implicit class LogicOps[A](fa: F[A]) {
    def accept(implicit FM: FlatMap[F]): F[Option[Unit]] =
      FlatMap[F].flatMap(fa)(_ => logic.accept)
    def accept(ctx: Context[F])(implicit FM: FlatMap[F]): F[(Context[F], Option[Unit])] =
      FlatMap[F].flatMap(fa)(_ => Applicative[F].pure(ctx -> Option(())))
  }

  override def receiveMessage(message: Message): OptionT[BotStateF, Unit] = unit

  override def receiveEditedMessage(editedMessage: Message): OptionT[BotStateF, Unit] = unit

  override def receiveExtMessage(extMessage: (Message, Option[User])): OptionT[BotStateF, Unit] = unit

  override def receiveChannelPost(message: Message): OptionT[BotStateF, Unit] = unit

  override def receiveEditedChannelPost(message: Message): OptionT[BotStateF, Unit] = unit

  override def receiveInlineQuery(inlineQuery: InlineQuery): OptionT[BotStateF, Unit] = unit

  override def receiveChosenInlineResult(chosenInlineResult: ChosenInlineResult): OptionT[BotStateF, Unit] = unit

  override def receiveCallbackQuery(callbackQuery: CallbackQuery): OptionT[BotStateF, Unit] = unit

  override def receiveShippingQuery(shippingQuery: ShippingQuery): OptionT[BotStateF, Unit] = unit

  override def receivePreCheckoutQuery(preCheckoutQuery: PreCheckoutQuery): OptionT[BotStateF, Unit] = unit

  override val handleState: Logic.StateCheck = Logic.StateCheck(_ == UserState.empty)
}

object Logic {
  type StateCheck = ru.ajieks.london.bot.Logic.StateCheck
  object StateCheck {
    def apply(f: UserState => Boolean): StateCheck = ReaderT[Id, UserState, Boolean](f)
  }
}
