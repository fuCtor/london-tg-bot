package ru.ajieks.london.bot.logic

import cats.Monad
import cats.data.OptionT
import com.bot4s.telegram.methods.SendMessage
import com.bot4s.telegram.models.Message
import ru.ajieks.london.bot.BotState
import ru.ajieks.london.bot.model.UserState

import scala.language.higherKinds

class FlipFlop[F[_]: Monad] extends Logic[F] {
  override def receiveMessage(message: Message): OptionT[BotStateF, Unit] = OptionT[BotStateF, Unit] {
    BotState { ctx =>
      message.text match {
        case Some("!flipflop") =>
          ctx.userState match {
            case UserState.FlipFlop() =>
              ctx.bot.request(SendMessage(message.chat.id, "flop")).accept(ctx.withState(UserState.empty))
            case _ =>
              ctx.bot.request(SendMessage(message.chat.id, "flip")).accept(ctx.withState(UserState.FlipFlop()))
          }
        case _ =>
          skip(ctx)
      }
    }
  }

  override val handleState: Logic.StateCheck = Logic.StateCheck(Seq(UserState.empty, UserState.FlipFlop()).contains(_))
}

object FlipFlop {
  def apply[F[_]: Monad]: FlipFlop[F] = new FlipFlop()
}



