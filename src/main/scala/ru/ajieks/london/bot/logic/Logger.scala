package ru.ajieks.london.bot.logic

import cats.Monad
import cats.data.OptionT
import com.bot4s.telegram.models.{CallbackQuery, Message}
import com.typesafe.scalalogging.StrictLogging
import ru.ajieks.london.bot.BotState

import scala.language.higherKinds

class Logger[F[_]: Monad] extends Logic[F] with StrictLogging {
  override def receiveMessage(message: Message): OptionT[BotStateF, Unit] = OptionT[BotStateF, Unit] {
    BotState.liftA { ctx =>
      logger.debug("[Ctx: {}] Receive: {}", ctx.userState, message.text)
      skip
    }
  }

  override def receiveCallbackQuery(callbackQuery: CallbackQuery): OptionT[BotStateF, Unit] = OptionT[BotStateF, Unit] {
    BotState.liftA { ctx =>
      logger.debug("[Ctx: {}] Receive QueryData: {}", ctx.userState, callbackQuery.data)
      skip
    }
  }

  override val handleState: Logic.StateCheck = Logic.StateCheck(_ => true)
}

object Logger {
  def apply[F[_]: Monad]: Logger[F] = new Logger
}
