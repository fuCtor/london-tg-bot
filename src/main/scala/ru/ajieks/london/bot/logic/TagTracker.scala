package ru.ajieks.london.bot.logic

import cats.Monad
import cats.data.OptionT
import com.bot4s.telegram.methods.SendMessage
import com.bot4s.telegram.models.Message
import com.typesafe.scalalogging.StrictLogging
import ru.ajieks.london.bot.{BotState, ChatId}
import ru.ajieks.london.bot.model.Tag
import ru.ajieks.london.bot.storage.TagStorage

import scala.language.higherKinds
import scala.util.matching.Regex

class TagTracker[F[_]: Monad](storage: TagStorage[F]) extends Logic[F] with StrictLogging {
  import cats.syntax.flatMap._
  override def receiveMessage(message: Message): OptionT[BotStateF, Unit] = OptionT[BotStateF, Unit] {
    BotState.liftA { ctx =>
      message.text.fold(skip)( text =>
        TagTracker.showTagRgx.findFirstMatchIn(text).map({ m =>
          message.replyToMessage match {
            case Some(baseMessage) =>
              saveTaggedMessage(baseMessage, m.group(1))
            case None =>
              storage.get(ChatId(message.chat.id), m.group(1)).flatMap({
                case Some(storedTag) =>
                  val msg = SendMessage(message.chat.id, "Наверно это?", replyToMessageId = Some(storedTag.messageId.toInt))
                  ctx.bot.request(msg).accept
                case _ => accept
              })
          }
        }).orElse(TagTracker.newTagRgx.findFirstMatchIn(text).map({ m =>
          saveTaggedMessage(message, m.group(1))
        })).getOrElse(skip)
      )
    }
  }

  private def saveTaggedMessage(message: Message, key:String): F[Option[Unit]] = {
    val tag = Tag(None, message.chat.id, message.messageId.toLong, message.from.map(_.id.toLong).getOrElse(0), key)
    storage.put(tag).accept
  }
}

object TagTracker {
  val newTagRgx: Regex = "\\#([\\w\\p{IsCyrillic}]+)".r
  val showTagRgx: Regex = "^\\#([\\w\\p{IsCyrillic}]+)$".r

  def apply[F[_]: Monad](storage: TagStorage[F]): TagTracker[F] = new TagTracker(storage)
}
