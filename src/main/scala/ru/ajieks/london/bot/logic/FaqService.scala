package ru.ajieks.london.bot.logic

import cats.Monad
import cats.data.OptionT
import com.bot4s.telegram.methods.{DeleteMessage, ParseMode, SendMessage}
import com.bot4s.telegram.models.{CallbackQuery, InlineKeyboardButton, InlineKeyboardMarkup, Message}
import com.typesafe.scalalogging.StrictLogging
import ru.ajieks.london.bot.{BotState, ChatId}
import ru.ajieks.london.bot.model.Faq
import ru.ajieks.london.bot.storage.FaqStorage

import scala.language.higherKinds
import scala.util.matching.Regex

class FaqService[F[_] : Monad](storage: FaqStorage[F]) extends Logic[F] with StrictLogging {

  import cats.syntax.applicative._
  import cats.syntax.flatMap._

  override def receiveMessage(message: Message): OptionT[BotStateF, Unit] = OptionT[BotStateF, Unit] {
    BotState.liftA { ctx =>
      message.text.fold(skip)(text =>
        FaqService.newFaqRgx.findFirstMatchIn(text).map({ m =>
          val messageWithText = for {
            msg <- message.replyToMessage
            txt <- msg.text
          } yield msg -> txt

          messageWithText match {
            case Some((baseMessage, text)) =>
              saveFaq(baseMessage, m.group(1), text)
                .flatMap(ctx.bot.request(_))
                .accept
            case None =>
              val msg = SendMessage(message.source, s"Не вижу описания 😞, повтори ответом на необходимое сообщение", replyToMessageId = Some(message.messageId))
              ctx.bot.request(msg).accept
          }
        }).orElse(FaqService.showFaqListRgx.findFirstMatchIn(text).map(_ =>
          storage.list(ChatId(message.source), 0).flatMap({ faqs =>
            val buttons = faqs.map(faq =>
              InlineKeyboardButton.callbackData(faq.caption, s"faq:show:${faq.id.getOrElse(0L)}")
            )
            val markup = InlineKeyboardMarkup.singleColumn(buttons)

            val msg = SendMessage(message.source, "Что посмотреть?", replyMarkup = Some(markup))
            ctx.bot.request(msg)
          }).accept
        )).getOrElse(skip)
      )
    }
  }

  override def receiveCallbackQuery(callbackQuery: CallbackQuery): OptionT[BotStateF, Unit] = OptionT[BotStateF, Unit] {
    BotState.liftA { ctx =>
      callbackQuery.data.fold(skip)( _.split(':').toList match {
        case "faq":: "show" :: id :: Nil =>
          val source = callbackQuery.message.fold(callbackQuery.from.id.toLong)(_.source)
          callbackQuery.message.fold(true.pure[F])(m =>
            ctx.bot.request(DeleteMessage(source, m.messageId))
          ).flatMap(_ =>
            storage.get(id.toLong).flatMap({
              case Some(faq) =>
                logger.debug(s"Finded ${faq}")
                val text = s"💬*${faq.caption}*\n${faq.text}"
                val msg = SendMessage(source, text, parseMode = Some(ParseMode.Markdown))
                ctx.bot.request(msg)
              case _ =>
                logger.debug("=((")
                ctx.bot.request(SendMessage(source, s"Что-то пошло не так 😞"))
            })
          ).accept
        case _ => skip
      })
    }
  }

  def saveFaq(message: Message, caption: String, text: String): F[SendMessage] = {
    val faq = Faq(None, message.source, caption, text)
    storage.put(faq).flatMap({
      case true => s"Спасибо, запомнил про: ${caption}".pure[F]
      case false => s"Что-то пошло не так 😞, попробуй попозже еще раз".pure[F]
    }).flatMap(msg =>
      SendMessage(message.source, msg, replyToMessageId = Some(message.messageId)).pure[F]
    )
  }
}

object FaqService {
  val newFaqRgx: Regex = "\\/faq\\s(.+)".r
  val showFaqListRgx: Regex = "^\\/faq$".r

  def apply[F[_] : Monad](storage: FaqStorage[F]): FaqService[F] = new FaqService(storage)
}
