package ru.ajieks.london.bot.model

case class Tag(id: Option[Long], chatId: Long, messageId: Long, userId: Long, tag: String)
