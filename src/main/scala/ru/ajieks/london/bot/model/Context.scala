package ru.ajieks.london.bot.model

import com.bot4s.telegram.api.{BotBase => TelegramBotBase}

import scala.language.higherKinds

class Context[F[_]]private(val bot: TelegramBotBase[F], val userState: UserState) {
  def withState(newState: UserState): Context[F] = Context(bot, newState)
}

object Context {
  def apply[F[_]](bot: TelegramBotBase[F], userState: UserState): Context[F] = new Context(bot, userState)
}
