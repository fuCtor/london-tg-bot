package ru.ajieks.london.bot.model

case class YandexLocation(id: Option[Long], chatId: Long, caption: String, longitude: Double, latitude: Double)
