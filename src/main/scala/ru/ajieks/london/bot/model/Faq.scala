package ru.ajieks.london.bot.model

case class Faq(id: Option[Long], chatId: Long, caption: String, text: String)
