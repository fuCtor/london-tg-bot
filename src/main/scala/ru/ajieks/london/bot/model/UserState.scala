package ru.ajieks.london.bot.model

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._

sealed trait UserState

object UserState {
  case class Empty() extends UserState
  val empty = Empty()

  case class FlipFlop() extends UserState
  case class Faq(messageId: Int, caption: String) extends UserState

  implicit val encoder : Encoder[UserState] = deriveEncoder[UserState]
  implicit val decoder : Decoder[UserState] = deriveDecoder[UserState]
}
