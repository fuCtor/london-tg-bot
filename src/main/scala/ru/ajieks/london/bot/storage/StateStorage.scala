package ru.ajieks.london.bot.storage

import ru.ajieks.london.bot.UserId
import ru.ajieks.london.bot.model.UserState

import scala.language.higherKinds

trait StateStorage[F[_]] {
  def get(userId: UserId): F[Option[UserState]]
  def put(userId: UserId, state: UserState): F[Unit]
}
