package ru.ajieks.london.bot.storage

import java.net.InetSocketAddress

import cats.effect.Async
import ru.ajieks.london.bot.UserId
import ru.ajieks.london.bot.model.UserState
import com.twitter.finagle.Redis
import com.twitter.io.Buf
import io.circe.jackson
import io.circe.syntax._

import scala.language.higherKinds

class RedisStateStorage[F[_]: Async](redisAddress: InetSocketAddress) extends StateStorage[F] {

  private val redis = Redis.newRichClient(s"${redisAddress.getHostString}:${redisAddress.getPort}")

  override def get(userId: UserId): F[Option[UserState]] = Async[F].async{ cb =>
    val _ = redis.get(key(userId)).map(_.flatMap({
      case Buf.Utf8(data) => jackson.decode[UserState](data).toOption
    }))
      .onSuccess(r => cb(Right(r)))
      .onFailure(r => cb(Left(r)))
  }

  override def put(userId: UserId, state: UserState): F[Unit] = Async[F].async { cb =>
    val _ = redis.set(key(userId), Buf.Utf8(state.asJson.noSpaces))
      .onSuccess(r => cb(Right(r)))
      .onFailure(r => cb(Left(r)))
  }

  def key(userId: UserId): Buf = Buf.Utf8(s"tg:bot:user:${userId}")
}

object RedisStateStorage {
  def apply[F[_]:Async](redisAddress: InetSocketAddress): RedisStateStorage[F] = new RedisStateStorage(redisAddress)
}
