package ru.ajieks.london.bot.storage

import cats.effect.Sync
import cats.~>
import com.typesafe.scalalogging.StrictLogging
import com.zaxxer.hikari.HikariDataSource
import io.getquill.context.jdbc.{Decoders, Encoders}
import io.getquill.context.monix.Runner
import io.getquill.{PostgresMonixJdbcContext, SnakeCase}
import monix.eval.Task
import monix.execution.Scheduler
import ru.ajieks.london.bot.ChatId
import ru.ajieks.london.bot.model.Faq

import scala.language.higherKinds

class DbFaqStorage[F[_]: Sync](ds: HikariDataSource)(implicit FK: Task ~> F) extends FaqStorage[F] with StrictLogging {

  lazy val ctx = new PostgresMonixJdbcContext(SnakeCase, ds, Runner.using(Scheduler.io())) with Decoders with Encoders with PgCodec
  import ctx._

  lazy val faqs: Quoted[EntityQuery[Faq]] = quote {
    querySchema[Faq]("faqs")
  }

  override def get(id: Long): F[Option[Faq]] = Sync[F].defer({
    logger.info(s"Get FAQ ${id}")
    val q = quote {
      faqs.filter(_.id.exists(_ == lift(id))).take(1)
    }

    FK(ctx.run(q).map(_.headOption))
  })

  override def put(faq: Faq): F[Boolean] = Sync[F].defer({
    logger.info(s"Save ${faq}")
    val q = quote {
      faqs.insert(
        _.chatId -> lift(faq.chatId),
        _.caption -> lift(faq.caption),
        _.text -> lift(faq.text)
      )
    }
    FK(ctx.run(q).map(_ > 0))
  })

  override def list(chatId: ChatId, page: Index): F[List[Faq]] = Sync[F].defer({
    logger.info(s"List FAQ in ${chatId}")
    val q = quote {
      faqs.filter(_.chatId == lift(chatId.toLong))
    }
    FK(ctx.run(q))
  })
}

object DbFaqStorage {
  def apply[F[_]: Sync](ds: HikariDataSource)(implicit FK: Task ~> F): DbFaqStorage[F] = new DbFaqStorage(ds)

}


