package ru.ajieks.london.bot.storage

import ru.ajieks.london.bot.ChatId
import ru.ajieks.london.bot.model.Faq

import scala.language.higherKinds

trait FaqStorage[F[_]] {
  def get(id: Long): F[Option[Faq]]
  def put(faq: Faq): F[Boolean]
  def list(chatId: ChatId, page: Int): F[List[Faq]]
}
