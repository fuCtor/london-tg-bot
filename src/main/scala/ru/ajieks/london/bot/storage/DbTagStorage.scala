package ru.ajieks.london.bot.storage

import cats.effect.Sync
import cats.~>
import com.typesafe.scalalogging.StrictLogging
import com.zaxxer.hikari.HikariDataSource
import io.getquill.context.jdbc.{Decoders, Encoders}
import io.getquill.context.monix.Runner
import io.getquill.{PostgresMonixJdbcContext, SnakeCase}
import monix.eval.Task
import monix.execution.Scheduler
import ru.ajieks.london.bot.ChatId
import ru.ajieks.london.bot.model.Tag

import scala.language.higherKinds

class DbTagStorage[F[_]: Sync](ds: HikariDataSource)(implicit FK: Task ~> F) extends TagStorage[F] with StrictLogging {

  lazy val ctx = new PostgresMonixJdbcContext(SnakeCase, ds, Runner.using(Scheduler.io())) with Decoders with Encoders with PgCodec
  import ctx._

  lazy val tags: Quoted[EntityQuery[Tag]] = quote {
    querySchema[Tag]("tags")
  }

  override def get(chatId: ChatId, tag: String): F[Option[Tag]] = Sync[F].defer({
    logger.info(s"Get from ${chatId} with ${tag} ")
    val q = quote {
      tags.filter({ rec =>
        rec.tag == lift(tag) && rec.chatId == lift(chatId.toLong)
      }).take(1)
    }

    FK(ctx.run(q).map(_.headOption))
  })

  override def put(tag: Tag): F[Boolean] = Sync[F].defer({
    logger.info(s"Save ${tag}")
    val q = quote {
      tags.insert(
        _.messageId -> lift(tag.messageId),
        _.chatId -> lift(tag.chatId),
        _.tag -> lift(tag.tag),
        _.userId -> lift(tag.userId)
      )
    }

    FK(ctx.run(q).map(_ > 0))
  })
}

object DbTagStorage {
  def apply[F[_]: Sync](ds: HikariDataSource)(implicit FK: Task ~> F): DbTagStorage[F] = new DbTagStorage(ds)
}
