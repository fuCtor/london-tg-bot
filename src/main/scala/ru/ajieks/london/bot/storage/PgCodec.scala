package ru.ajieks.london.bot.storage

import java.time.Instant

import io.circe.{Json, jackson}
import io.getquill.PostgresMonixJdbcContext

trait PgCodec {
  this: PostgresMonixJdbcContext[_] =>

  implicit val encodeInstant: Encoder[Instant] = encoder(java.sql.Types.TIMESTAMP, (index, value, row) =>
    row.setObject(index, value.getEpochSecond, java.sql.Types.TIMESTAMP))

  implicit val decoderInstant: Decoder[Instant] = decoder((index, row) =>
    Instant.ofEpochSecond(row.getObject(index, classOf[Long]))
  )

  implicit val encodeJson: Encoder[Json] = encoder(java.sql.Types.OTHER, (index, value, row) =>
    row.setObject(index, value.noSpaces, java.sql.Types.OTHER))

  implicit val decodeJson: Decoder[Json] = decoder((index, row) =>
    jackson.parse(row.getObject(index).toString).getOrElse(Json.Null))
}
