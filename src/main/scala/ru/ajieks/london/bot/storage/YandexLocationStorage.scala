package ru.ajieks.london.bot.storage

import ru.ajieks.london.bot.ChatId
import ru.ajieks.london.bot.model.YandexLocation

import scala.language.higherKinds

trait YandexLocationStorage[F[_]] {
  def get(id: Long): F[Option[YandexLocation]]
  def put(location: YandexLocation): F[Boolean]
  def list(chatId: ChatId, page: Int): F[List[YandexLocation]]
}
