package ru.ajieks.london.bot.storage

import ru.ajieks.london.bot.ChatId
import ru.ajieks.london.bot.model.Tag

import scala.language.higherKinds

trait TagStorage[F[_]] {
  def get(chatId: ChatId, tag: String): F[Option[Tag]]
  def put(tag: Tag): F[Boolean]
}
