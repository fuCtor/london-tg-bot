package ru.ajieks.london.bot.storage

import cats.effect.Sync
import cats.~>
import com.typesafe.scalalogging.StrictLogging
import com.zaxxer.hikari.HikariDataSource
import io.getquill.context.jdbc.{Decoders, Encoders}
import io.getquill.context.monix.Runner
import io.getquill.{PostgresMonixJdbcContext, SnakeCase}
import monix.eval.Task
import monix.execution.Scheduler
import ru.ajieks.london.bot.ChatId
import ru.ajieks.london.bot.model.YandexLocation

import scala.language.higherKinds

class DbYandexLocationStorage[F[_]: Sync](ds: HikariDataSource)(implicit FK: Task ~> F) extends YandexLocationStorage[F] with StrictLogging {

  lazy val ctx = new PostgresMonixJdbcContext(SnakeCase, ds, Runner.using(Scheduler.io())) with Decoders with Encoders with PgCodec
  import ctx._

  lazy val locations: Quoted[EntityQuery[YandexLocation]] = quote {
  querySchema[YandexLocation]("yandex_locations")
}
  def get(id: Long): F[Option[YandexLocation]] = Sync[F].defer {
    logger.info(s"Get FAQ ${id}")
    val q = quote {
      locations.filter(_.id.exists(_ == lift(id))).take(1)
    }

    FK(ctx.run(q).map(_.headOption))
  }

  def put(location: YandexLocation): F[Boolean] = Sync[F].defer({
    logger.info(s"Save ${location}")
    val q = quote {
      locations.insert(
        _.chatId -> lift(location.chatId),
        _.caption -> lift(location.caption),
        _.longitude -> lift(location.longitude),
        _.latitude -> lift(location.latitude)
      )
    }
    FK(ctx.run(q).map(_ > 0))
  })

  def list(chatId: ChatId, page: Int): F[List[YandexLocation]] = Sync[F].defer({
    logger.info(s"List Locations in ${chatId}")
    val q = quote {
      locations.filter(_.chatId == lift(chatId.toLong))
    }
    FK(ctx.run(q))
  })
}

object DbYandexLocationStorage {
  def apply[F[_]: Sync](ds: HikariDataSource)(implicit FK: Task ~> F): DbYandexLocationStorage[F] = new DbYandexLocationStorage(ds)
}
