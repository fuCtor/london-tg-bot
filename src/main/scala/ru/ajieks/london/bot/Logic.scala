package ru.ajieks.london.bot

import cats.data.{OptionT, ReaderT}
import cats.{Id, Monad}
import com.bot4s.telegram.models._
import ru.ajieks.london.bot.model.UserState

import scala.language.higherKinds

trait Logic[F[_]] {
  type BotStateF[A] = BotState[F, A]
  def receiveMessage(message: Message): OptionT[BotStateF, Unit]
  def receiveEditedMessage(editedMessage: Message): OptionT[BotStateF, Unit]
  def receiveExtMessage(extMessage: (Message, Option[User])): OptionT[BotStateF, Unit]

  def receiveChannelPost(message: Message): OptionT[BotStateF, Unit]
  def receiveEditedChannelPost(message: Message): OptionT[BotStateF, Unit]

  def receiveInlineQuery(inlineQuery: InlineQuery): OptionT[BotStateF, Unit]
  def receiveChosenInlineResult(chosenInlineResult: ChosenInlineResult): OptionT[BotStateF, Unit]

  def receiveCallbackQuery(callbackQuery: CallbackQuery): OptionT[BotStateF, Unit]

  def receiveShippingQuery(shippingQuery: ShippingQuery): OptionT[BotStateF, Unit]
  def receivePreCheckoutQuery(preCheckoutQuery: PreCheckoutQuery): OptionT[BotStateF, Unit]

  def handleState: Logic.StateCheck
}

object Logic {
  type StateCheck = ReaderT[Id, UserState, Boolean]
  def merge[F[_]: Monad](logic: Logic[F]*): Logic[F] = new Logic[F] {
    
    private def withFilter(f: Logic[F] => OptionT[BotStateF, Unit]): OptionT[BotStateF, Unit] = OptionT[BotStateF, Unit] {
      BotState { ctx =>
        logic
          .filter(_.handleState(ctx.userState))
          .foldLeft(OptionT.none[BotStateF, Unit])((r, l) => r.orElse(f(l)))
          .value.run(ctx)
      }
    }
    
    override def receiveMessage(message: Message): OptionT[BotStateF, Unit] =
      withFilter(_.receiveMessage(message))

    override def receiveEditedMessage(editedMessage: Message): OptionT[BotStateF, Unit] =
      withFilter(_.receiveEditedMessage(editedMessage))

    override def receiveExtMessage(extMessage: (Message, Option[User])): OptionT[BotStateF, Unit] =
      withFilter(_.receiveExtMessage(extMessage))

    override def receiveChannelPost(message: Message): OptionT[BotStateF, Unit] =
      withFilter(_.receiveChannelPost(message))

    override def receiveEditedChannelPost(message: Message): OptionT[BotStateF, Unit] =
      withFilter(_.receiveEditedChannelPost(message))

    override def receiveInlineQuery(inlineQuery: InlineQuery): OptionT[BotStateF, Unit] =
      withFilter(_.receiveInlineQuery(inlineQuery))

    override def receiveChosenInlineResult(chosenInlineResult: ChosenInlineResult): OptionT[BotStateF, Unit] =
      withFilter(_.receiveChosenInlineResult(chosenInlineResult))

    override def receiveCallbackQuery(callbackQuery: CallbackQuery): OptionT[BotStateF, Unit] =
      withFilter(_.receiveCallbackQuery(callbackQuery))

    override def receiveShippingQuery(shippingQuery: ShippingQuery): OptionT[BotStateF, Unit] =
      withFilter(_.receiveShippingQuery(shippingQuery))

    override def receivePreCheckoutQuery(preCheckoutQuery: PreCheckoutQuery): OptionT[BotStateF, Unit] =
      withFilter(_.receivePreCheckoutQuery(preCheckoutQuery))

    override val handleState: ReaderT[Id, UserState, Boolean] =
      ReaderT[Id, UserState, Boolean](state => logic.exists(_.handleState(state)))
  }
}
