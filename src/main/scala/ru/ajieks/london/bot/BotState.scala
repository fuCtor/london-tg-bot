package ru.ajieks.london.bot

import cats.data.StateT
import cats.{Applicative, FlatMap}
import ru.ajieks.london.bot.model.Context

import scala.language.higherKinds

object BotState {
  def pure[F[_]: Applicative, A](v: A): BotState[F, A] = StateT.pure(v)
  def apply[F[_]: Applicative, A](f: Context[F] => F[(Context[F], A)]): BotState[F, A] = StateT(f)

  def get[F[_]]()(implicit F: Applicative[F]): BotState[F, Context[F]] = StateT.get

  def liftF[F[_]: Applicative, A](fa: F[A]): BotState[F, A] = StateT.liftF(fa)
  def liftA[F[_]: Applicative : FlatMap, A](f: Context[F] => F[A]): BotState[F, A] =
    StateT(ctx =>
      FlatMap[F].flatMap(f(ctx))(r => Applicative[F].pure(ctx -> r))
    )
}

