package ru.ajieks.london

import cats.data.StateT
import ru.ajieks.london.bot.model.Context
import supertagged.TaggedType

import scala.language.higherKinds

package object bot {
  type BotState[F[_], A] = StateT[F, Context[F], A]

  object UserId extends TaggedType[Int]
  type UserId = UserId.Type

  object MessageId extends TaggedType[Long]
  type MessageId = MessageId.Type

  object ChatId extends TaggedType[Long]
  type ChatId = ChatId.Type
}
