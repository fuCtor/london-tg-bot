package ru.ajieks.london

import cats.effect.{Async, Resource, Sync}
import com.bot4s.telegram.cats.TelegramBot
import com.bot4s.telegram.models._
import com.softwaremill.sttp.SttpBackend
import ru.ajieks.london.bot.storage.StateStorage
import ru.ajieks.london.bot.{BotBase, Logic}
import ru.ajieks.london.config.Telegram

import scala.language.higherKinds

class Bot[F[_]: Async](config: Telegram, logic: Logic[F], backend: SttpBackend[F, Nothing], val stateStorage: StateStorage[F]) extends TelegramBot(config.token, backend) with BotBase[F] {
  import cats.syntax.applicative._
  import cats.syntax.flatMap._
  override def receiveMessage(msg: Message): F[Unit] =
    execute(msg.from, logic.receiveMessage(msg)).flatMap( _ => ().pure)
  override def receiveEditedMessage(editedMessage: Message): F[Unit] =
    execute(editedMessage.from, logic.receiveEditedMessage(editedMessage)).flatMap( _ => ().pure)
  override def receiveExtMessage(extMessage: (Message, Option[User])): F[Unit] =
    execute(extMessage._1.from, logic.receiveExtMessage(extMessage)).flatMap( _ => ().pure)

  override def receiveChannelPost(message: Message): F[Unit] =
    execute(message.from, logic.receiveChannelPost(message)).flatMap( _ => ().pure)
  override def receiveEditedChannelPost(message: Message): F[Unit] =
    execute(message.from, logic.receiveEditedChannelPost(message)).flatMap( _ => ().pure)

  override def receiveInlineQuery(inlineQuery: InlineQuery): F[Unit] =
    execute(Some(inlineQuery.from), logic.receiveInlineQuery(inlineQuery)).flatMap( _ => ().pure)
  override def receiveChosenInlineResult(chosenInlineResult: ChosenInlineResult): F[Unit] =
    execute(Some(chosenInlineResult.from), logic.receiveChosenInlineResult(chosenInlineResult)).flatMap( _ => ().pure)

  override def receiveCallbackQuery(callbackQuery: CallbackQuery): F[Unit] =
    execute(Some(callbackQuery.from), logic.receiveCallbackQuery(callbackQuery)).flatMap( _ => ().pure)

  override def receiveShippingQuery(shippingQuery: ShippingQuery): F[Unit] =
    execute(Some(shippingQuery.from), logic.receiveShippingQuery(shippingQuery)).flatMap( _ => ().pure)
  override def receivePreCheckoutQuery(preCheckoutQuery: PreCheckoutQuery): F[Unit] =
    execute(Some(preCheckoutQuery.from), logic.receivePreCheckoutQuery(preCheckoutQuery)).flatMap( _ => ().pure)
}

object Bot {
  def apply[F[_] : Async](config: Telegram, logic: Logic[F],  backend: SttpBackend[F, Nothing], stateStorage: StateStorage[F]): Resource[F, Bot[F]] = Resource.liftF(
    Sync[F].delay(
      new Bot[F](config, logic, backend, stateStorage)
    )
  )
}
