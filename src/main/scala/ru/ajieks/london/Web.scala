package ru.ajieks.london

import cats.effect.{Resource, Sync}

import scala.language.higherKinds

class Web[F[_]] {

}

object Web {
  def apply[F[_] : Sync](): Resource[F, Web[F]] = Resource.liftF(
    Sync[F].delay(
      new Web[F]
    )
  )
}