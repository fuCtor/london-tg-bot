package ru.ajieks.london.storage

import cats.effect.{Resource, Sync}
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import liquibase.Liquibase
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.ClassLoaderResourceAccessor
import ru.ajieks.london.config.{Db => DbConfig}

import scala.language.higherKinds

object Db {
  def apply[F[_] : Sync](config: DbConfig): Resource[F, HikariDataSource] =
    Resource.fromAutoCloseable(Sync[F].defer {
      import cats.syntax.applicative._
      import cats.syntax.flatMap._
      val hikariConfig: HikariConfig = {
        val cfg = new HikariConfig()
        cfg.setJdbcUrl(config.jdbcUrl)
        config.user.foreach(cfg.setUsername)
        config.password.foreach(cfg.setPassword)
        cfg.setMaximumPoolSize(config.connectionPool)
        cfg.setAutoCommit(true)
        cfg.addDataSourceProperty("cachePrepStmts", "true")
        cfg.addDataSourceProperty("prepStmtCacheSize", "250")
        cfg.addDataSourceProperty("prepStmtCacheSqlLimit", "2048")
        cfg
      }
      val ds = new HikariDataSource(hikariConfig)
      migrate(ds, "db/main.changelog.sql").flatMap(_ => ds.pure[F])
    })

  def migrate[F[_] : Sync](ds: HikariDataSource, diffFilePath: String): F[Unit] =
    Sync[F].bracket(Sync[F].delay(ds.getConnection))({ con =>
      import cats.syntax.applicative._

      Resource.make(Sync[F].delay {
        val database = DatabaseFactory.getInstance.findCorrectDatabaseImplementation(new JdbcConnection(con))
        val classLoader = classOf[DbConfig].getClassLoader
        val resourceAccessor = new ClassLoaderResourceAccessor(classLoader)
        new Liquibase(diffFilePath, resourceAccessor, database)
      })(_.forceReleaseLocks().pure[F]).use(_.update(null: String).pure[F])
    })(con => Sync[F].delay(con.close()))
}
