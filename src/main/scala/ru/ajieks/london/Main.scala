package ru.ajieks.london

import cats.arrow.FunctionK
import cats.effect._
import cats.implicits._
import com.softwaremill.sttp.asynchttpclient.monix.AsyncHttpClientMonixBackend
import monix.eval._
import ru.ajieks.london.bot.Logic
import ru.ajieks.london.bot.storage.{DbFaqStorage, DbTagStorage, DbYandexLocationStorage, RedisStateStorage}
import ru.ajieks.london.bot.logic._
import ru.ajieks.london.storage.Db

object Main extends TaskApp {
  implicit val indentK = FunctionK.id[Task]
  override def run(args: List[String]): Task[ExitCode] = {
    val app: Resource[Task, Bot[Task]] = for {
      config <- config.Main.res
      db <- Db(config.db)
      tagStorage = DbTagStorage(db)
      faqStorage = DbFaqStorage(db)
      yaLocStorage = DbYandexLocationStorage(db)
      stateStorage = RedisStateStorage(config.redis.address)
      logic = Logic.merge(Logger[Task], TagTracker[Task](tagStorage), FaqService[Task](faqStorage), Yandex[Task](yaLocStorage), Echo[Task], FlipFlop[Task])
      bot <- Bot(config.telegram, logic, AsyncHttpClientMonixBackend(), stateStorage)
      _ <- Web()
    } yield bot

    app.use(_.run()).as(ExitCode.Success)
  }
}
