package ru.ajieks.london.config

case class Db(jdbcUrl: String, user: Option[String], password: Option[String], connectionPool: Int)
