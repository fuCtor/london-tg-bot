package ru.ajieks.london.config

import java.net.InetSocketAddress

import cats.effect.{Resource, Sync}
import pureconfig._
import pureconfig.generic.auto._

import scala.language.higherKinds
import scala.util.Try

case class Main(telegram: Telegram, redis: Redis, db: Db)

object Main {
  implicit val socketReader = pureconfig.ConfigReader.fromStringTry(str => Try({
    str.split(':') match {
      case Array(host) => new InetSocketAddress(host, 0)
      case Array(host, port) => new InetSocketAddress(host, port.toInt)
    }
  }))

  def res[F[_]: Sync]: Resource[F, Main] = Resource.liftF(Sync[F].delay {
    ConfigSource.default.loadOrThrow[Main]
  })
}
