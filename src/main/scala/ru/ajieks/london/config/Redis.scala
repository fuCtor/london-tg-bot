package ru.ajieks.london.config

import java.net.InetSocketAddress

case class Redis(address: InetSocketAddress)
